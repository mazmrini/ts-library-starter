# Typescript library starter

## Scripts

#### Tests
```bash
npm test
```

#### Linter
```bash
npm run lint
```

#### Coverage
```bash
npm run testWithCoverage
```

#### Build
```bash
npm run build
```