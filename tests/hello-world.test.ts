import { describe, it } from 'mocha';
import { expect } from 'chai';
import { hello, world } from '../src/hello-world';

describe('hello-world', () => {
    it('hello() should return hello', () => {
        const result = hello();

        expect(result).to.be.equal('hello');
    });

    it('world() should return hello', () => {
        const result = world();

        expect(result).to.be.equal('world');
    });
});
